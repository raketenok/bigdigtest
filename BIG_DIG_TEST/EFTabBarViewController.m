//
//  EFTabBarViewController.m
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 10.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFTabBarViewController.h"

@interface EFTabBarViewController ()

@end

@implementation EFTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *selTab = [[UIImage imageNamed:@"Selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    CGSize tabSize = CGSizeMake(CGRectGetWidth(self.view.frame)/2, 49);

    UIGraphicsBeginImageContext(tabSize);
    [selTab drawInRect:CGRectMake(self.view.bounds.origin.x, 0, tabSize.width, tabSize.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setSelectionIndicatorImage:
     reSizeImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

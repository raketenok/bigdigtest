//
//  EFPostViewController.m
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 07.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFPostViewController.h"

@interface EFPostViewController ()

@property (strong,nonatomic) NSData* itemData;

@end

@implementation EFPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.arrayPhoto = [[NSMutableArray alloc]init];
    self.itemData = [[NSData alloc]init];
    
    [self loadsettings];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissKeyboardPhone {
    
    [self.descriptionLabel resignFirstResponder];
    
}

#pragma mark - Helper

-(void) loadsettings{
    
   
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"camera1"]];
    [self.addPhotoImage setBackgroundColor:background];
    [self.addPhotoImage2 setBackgroundColor:background];
    [self.addPhotoImage3 setBackgroundColor:background];
    [self.addPhotoImage setContentMode:UIViewContentModeScaleToFill];
    [self.addPhotoImage2 setContentMode:UIViewContentModeScaleToFill];
    [self.addPhotoImage3 setContentMode:UIViewContentModeScaleToFill];
    
    self.postButtonProperty.layer.cornerRadius = 6;
    self.descriptionLabel.layer.cornerRadius = 6;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboardPhone)];
    
    [self.view addGestureRecognizer:tap];
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    return YES;
}



- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
       [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];

    [self.view endEditing:YES];
    return YES;
}
#pragma mark - UITextViewDelegate


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    return YES;

    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
    [self.view endEditing:YES];
    return YES;
    
}


#pragma mark - ImagePicker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSLog(@"Photo from library %@",info);
    
    UIImage* image = [info valueForKey:UIImagePickerControllerOriginalImage];

    if ([self.arrayPhoto count]<3) {
        
        
        [self.arrayPhoto addObject:image];
        
        NSLog(@"!!!%@",self.arrayPhoto);
        
        if ([self.arrayPhoto count] == 2) {
            self.addPhotoImage2.image = [self.arrayPhoto objectAtIndex:1];
            
        } else if ([self.arrayPhoto count] == 3) {
            self.addPhotoImage3.image = [self.arrayPhoto objectAtIndex:2];
            
        } else {
            self.addPhotoImage.image = [self.arrayPhoto objectAtIndex:0];
            
        }
    }
    
    self.imageData = UIImageJPEGRepresentation(image, 0.0000001);
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Actions



- (IBAction)postAction:(UIButton *)sender {
    NSLog(@"ADD POST");
   
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.arrayPhoto];

    
    if (!self.detailItem) {
        EFItem* item =
        [NSEntityDescription insertNewObjectForEntityForName:@"EFItem"
                                      inManagedObjectContext:[[EFDataManager sharedManager] managedObjectContext]];

        item.labelItem = self.titleLabel.text;
        item.priceItem = self.priceTextLabel.text;
        item.descriptionItem = self.descriptionLabel.text;
        item.imageItem = [[NSData alloc]initWithData:data];
       
    }else{
        self.detailItem.labelItem = self.titleLabel.text;
        self.detailItem.priceItem = self.priceTextLabel.text;
        self.detailItem.descriptionItem = self.descriptionLabel.text;
        self.detailItem.imageItem = [[NSData alloc]initWithData:data];

    }
    [[[EFDataManager sharedManager] managedObjectContext] save:nil];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Excellent"
                                                                   message:@"Post is created"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {
    
    [self.navigationController popToRootViewControllerAnimated:YES];

                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}




- (IBAction)addPhotoAction:(UIButton *)sender {
    
    UIAlertController* alertController =
    [UIAlertController alertControllerWithTitle:nil
                                        message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIAlertAction* chooseFromGalleryAction =
    [UIAlertAction actionWithTitle:@"Выбрать из галереи"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               
                               UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                               imagePickerController.delegate = self;
                               imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                               
                               [self presentViewController:imagePickerController animated:YES completion:nil];
                           }];
    
    
    UIAlertAction* deletePhotoAction =
    [UIAlertAction actionWithTitle:@"Удалить"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               
                               if ([self.arrayPhoto count] != 0) {
                                   [self.arrayPhoto removeLastObject];
                                   if ([self.arrayPhoto count] == 2) {
                                       self.addPhotoImage3.image = nil;
                                   } else if ([self.arrayPhoto count] == 1){
                                       self.addPhotoImage2.image = nil;
                                   } else {
                                       self.addPhotoImage.image = nil;
                                   }
                               }
                               
                           }];
    
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Отмена"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    
    
    [alertController addAction:chooseFromGalleryAction];
    [alertController addAction:deletePhotoAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    alertController.view.tintColor = [UIColor blackColor];


}
@end

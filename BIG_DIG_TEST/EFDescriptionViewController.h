//
//  EFDescriptionViewController.h
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 08.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFDataManager.h"
#import "EFItem.h"

@interface EFDescriptionViewController : UIViewController <UIScrollViewDelegate ,NSFetchedResultsControllerDelegate>


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property(strong,nonatomic) EFItem* detailItem;
@property(strong,nonatomic) NSMutableArray *photoArray;
@property(strong,nonatomic) NSData *dataImage;

@property (strong,nonatomic) UIPageControl* pageControl;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteItemButton;
@property (weak, nonatomic) IBOutlet UIButton *editItemButton;


- (IBAction)deleteItemAction:(UIButton *)sender;




@end

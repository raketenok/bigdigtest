//
//  EFEditingTableViewController.h
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 08.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFDataManager.h"
#import "EFItem.h"

@interface EFEditingViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>


@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property(strong,nonatomic) EFItem* detailItem;
@property(strong,nonatomic) NSMutableArray *photoArray;
@property(strong,nonatomic) NSData *dataImage;



- (IBAction)editAction:(UIButton *)sender;
- (IBAction)addPhotoAction:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UITextField *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *priceLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImage;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImage2;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImage3;
@property (weak, nonatomic) IBOutlet UITextView *editButtonProperty;



@end

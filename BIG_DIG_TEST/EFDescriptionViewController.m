//
//  EFDescriptionViewController.m
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 08.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFDescriptionViewController.h"
#import "AppDelegate.h"

@interface EFDescriptionViewController ()




@end

@implementation EFDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self loadSettings];
    [self addScrollView];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Helper

-(void) loadSettings {
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"photoLarge"]];
    [self.scrollView setBackgroundColor:background];
    
    
    self.priceLabel.text = self.detailItem.priceItem;
    self.titleLabel.text = self.detailItem.labelItem;
    self.descriptionLabel.text = self.detailItem.descriptionItem;
    
    [self.editItemButton setTintColor:[UIColor whiteColor]];
    [self.deleteItemButton setTintColor:[UIColor whiteColor]];

    
    
}

-(void) addScrollView {
    
    NSData *newdata = [NSData dataWithData:self.detailItem.imageItem];
    self.photoArray = [NSMutableArray arrayWithArray: [NSKeyedUnarchiver unarchiveObjectWithData:newdata]];
    
    
    if ([self.photoArray count] > 0) {
        
        self.scrollView.delegate = self;

        self.scrollView.pagingEnabled = YES;
        
        int offset = 0;
        
        for (int i = 0; i < [self.photoArray count]; i++) {
            
            
            UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*(self.view.frame.size.width), self.scrollView.bounds.origin.y, self.view.frame.size.width, self.scrollView.frame.size.height)];
            
            [imageView setImage:[self.photoArray objectAtIndex:i]];
            
            [self.scrollView addSubview:imageView];
            
            offset = offset + self.view.frame.size.width;
            
        }
        
        self.scrollView.contentSize = CGSizeMake(offset,self.scrollView.frame.size.height);
        
    }
    
    [self.view addSubview:self.scrollView];
    
    
    //Page Control
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.scrollView.frame.size.width - 30, self.scrollView.frame.size.width, 20)];
    self.pageControl.numberOfPages = self.scrollView.contentSize.width/self.scrollView.frame.size.width;
    [self.pageControl addTarget:self action:@selector(pageChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:self.pageControl];
    
}

- (IBAction)pageChanged:(UIPageControl *)sender {
    
    NSInteger pageNumber = self.pageControl.currentPage;
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width*pageNumber;
    frame.origin.y=0;
    
    [self.scrollView scrollRectToVisible:frame animated:YES];
}

#pragma mark - UIScrollViewDelegate




-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView  {
    self.pageControl.currentPage = floorf(scrollView.contentOffset.x/scrollView.frame.size.width);
}
#pragma mark - Actions




- (IBAction)deleteItemAction:(UIButton *)sender {
    
    NSString *endString = [NSString stringWithFormat:@"Do you want to delete the item?"];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Are you sure"
                                                                   message:endString
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSManagedObject* detailItem = self.detailItem;
        
        NSManagedObjectContext* managedObjectContext = self.managedObjectContext;
        
        [managedObjectContext deleteObject:detailItem];
        
        NSError *error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Error");
        }
        
        [[[EFDataManager sharedManager]managedObjectContext] save:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    

}

#pragma mark - Segue

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    EFDescriptionViewController* vc = segue.destinationViewController;
    vc.detailItem = self.detailItem;
    vc.managedObjectContext = self.managedObjectContext;
    vc.photoArray = self.photoArray;
    vc.dataImage = self.dataImage;
    
}

@end

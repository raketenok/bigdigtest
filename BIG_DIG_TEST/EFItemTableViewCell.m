//
//  EFItemTableViewCell.m
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 07.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFItemTableViewCell.h"

@implementation EFItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"camera1"]];
    [self.itemImage setBackgroundColor:background];}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

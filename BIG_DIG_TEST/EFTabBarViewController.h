//
//  EFTabBarViewController.h
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 10.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EFTabBarViewController : UITabBarController

@end

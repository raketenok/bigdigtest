//
//  EFItemsTableViewController.m
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 07.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFItemsTableViewController.h"
#import "EFDataManager.h"
#import "EFItem.h"
#import "EFItemTableViewCell.h"
#import "EFDescriptionViewController.h"


@interface EFItemsTableViewController () 

@property(strong,nonatomic) NSIndexPath* selectedPath;
@property(strong,nonatomic) NSMutableArray *photoArray;
@property(strong,nonatomic) EFItemsTableViewController *searchTableViewController;


@property(strong,nonatomic) NSMutableArray *resultArray;
@property (nonatomic, retain) UISearchController *searchController;
@property (nonatomic, retain) NSString *searchString;

@property(strong,nonatomic) NSData *dataImage;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end

@implementation EFItemsTableViewController
@synthesize fetchedResultsController = _fetchedResultsController;


- (instancetype)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.resultArray = [[NSMutableArray alloc]init];


    [self.tableView reloadData];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EFItem" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor * name = [[NSSortDescriptor alloc]initWithKey:@"labelItem" ascending:YES
                               ];
    [fetchRequest setSortDescriptors:@[name]];
    
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}



- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedPath = indexPath;
    [self performSegueWithIdentifier:@"toDescriptionViewController" sender:nil];
    
}

#pragma mark - Segue

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    EFItem* item = [self.fetchedResultsController objectAtIndexPath:self.selectedPath];
    
    EFDescriptionViewController* vc = segue.destinationViewController;
    vc.detailItem = item;
    vc.managedObjectContext = self.managedObjectContext;
    vc.photoArray = self.photoArray;
    vc.dataImage = self.dataImage;
    
}


#pragma  mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
       [searchBar setShowsCancelButton:YES animated:YES];
    NSLog(@"START EDITING");

}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    
    NSLog(@"STOP EDITING");
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"EFItem" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    
    NSArray* searchResults = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if ([searchResults count] <= 0) {
        NSLog(@"No entity");
    }
    NSString* currentLetter ;
    
    for (NSManagedObject* object in searchResults) {
        
        currentLetter = [object valueForKey:@"labelItem"];
        NSLog(@"Item:  %@", currentLetter);
        
    }
    
    NSSortDescriptor * name = [[NSSortDescriptor alloc]initWithKey:@"labelItem" ascending:YES];
    [fetchRequest setSortDescriptors:@[name]];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest managedObjectContext:[[EFDataManager sharedManager] managedObjectContext] sectionNameKeyPath:nil cacheName:@"Master"];
    _fetchedResultsController.delegate = self;
    
    if (![self.fetchedResultsController performFetch:&error])
    {
        NSLog(@"Unresolved error %@, %@", [error localizedDescription], [error localizedFailureReason]);
        abort();
        
    }
    
    
    [self.tableView reloadData];
    
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"text changed %@", searchText);
    [self filterContentForSearchText:searchText];
    
    
}


- (void)filterContentForSearchText:(NSString*)searchText
{
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"EFItem" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"labelItem contains[cd] %@", self.searchBar.text];
    
    [fetchRequest setPredicate:predicate];
    
        NSError *error;
        
        NSArray* searchResults = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        if ([searchResults count] <= 0) {
            NSLog(@"No entity");
        }
            NSString* currentLetter ;
            
            for (NSManagedObject* object in searchResults) {
                
                currentLetter = [object valueForKey:@"labelItem"];
                NSLog(@"Item:  %@", currentLetter);
                
            }
        
        NSSortDescriptor * name = [[NSSortDescriptor alloc]initWithKey:@"labelItem" ascending:YES];
        [fetchRequest setSortDescriptors:@[name]];
        
        _fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:fetchRequest managedObjectContext:[[EFDataManager sharedManager] managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
        _fetchedResultsController.delegate = self;
        
        if (![self.fetchedResultsController performFetch:&error])
        {
            NSLog(@"Unresolved error %@, %@", [error localizedDescription], [error localizedFailureReason]);
            abort();
            
        }
    
          [self.tableView reloadData];
   
}



#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* identifier = @"Cell";
    
    EFItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    EFItem* object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
        self.dataImage = [NSData dataWithData:object.imageItem];
        self.photoArray = [NSMutableArray arrayWithArray: [NSKeyedUnarchiver unarchiveObjectWithData:self.dataImage]];

    cell.itemNameLabel.text = object.labelItem;
    cell.itemPriceLabel.text = object.priceItem;
    
    if ([self.photoArray count] > 0) {
        
        cell.itemImage.image = [self.photoArray objectAtIndex:0];
    } else{
        cell.itemImage.image = [UIImage imageNamed:@"camera1"];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - CoreData Methods
-(NSManagedObjectContext*) managedObjectContext{
    if (!_managedObjectContext) {
        _managedObjectContext = [[EFDataManager sharedManager]managedObjectContext];
    }
    return _managedObjectContext;
}

#pragma mark - Fetched results controller


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}



@end

//
//  EFDataManager.h
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 07.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EFDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;



+(EFDataManager*) sharedManager;

-(NSArray*) allObjects;
@end

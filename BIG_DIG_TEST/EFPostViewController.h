//
//  EFPostViewController.h
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 07.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFDataManager.h"
#import "EFItem.h"
#import "EFTabBarViewController.h"

@interface EFPostViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (strong,nonatomic) NSMutableArray* arrayPhoto;
@property (strong, nonatomic)NSData *imageData;
@property (strong,nonatomic) EFItem* detailItem;



- (IBAction)postAction:(UIButton *)sender;
- (IBAction)addPhotoAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *priceTextLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImage;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImage2;
@property (weak, nonatomic) IBOutlet UIImageView *addPhotoImage3;
@property (weak, nonatomic) IBOutlet UIButton *postButtonProperty;


@end

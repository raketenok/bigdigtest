//
//  EFEditingTableViewController.m
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 08.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFEditingViewController.h"

@interface EFEditingViewController ()

@end

@implementation EFEditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
 
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"camera1"]];
    [self.addPhotoImage setBackgroundColor:background];
    [self.addPhotoImage2 setBackgroundColor:background];
    [self.addPhotoImage3 setBackgroundColor:background];
    
    [self.addPhotoImage setContentMode:UIViewContentModeScaleToFill];
    [self.addPhotoImage2 setContentMode:UIViewContentModeScaleToFill];
    [self.addPhotoImage3 setContentMode:UIViewContentModeScaleToFill];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboardPhone)];
    
    [self.view addGestureRecognizer:tap];
    
    [self loadSettings];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) dismissKeyboardPhone {
    
    [self.descriptionLabel resignFirstResponder];
    
}

-(void) loadSettings{
    
    self.editButtonProperty.layer.cornerRadius = 6;
    self.descriptionLabel.layer.cornerRadius = 6;
    
    self.dataImage = [NSData dataWithData:self.detailItem.imageItem];
    self.photoArray = [NSMutableArray arrayWithArray: [NSKeyedUnarchiver unarchiveObjectWithData:self.dataImage]];
    
   
        if ([self.photoArray count] > 0) {
            
            if ([self.photoArray count] == 1) {
                self.addPhotoImage.image = [self.photoArray objectAtIndex:0];
                
            } else if ([self.photoArray count] == 2) {
                
                self.addPhotoImage.image = [self.photoArray objectAtIndex:0];
                self.addPhotoImage2.image = [self.photoArray objectAtIndex:1];
                
                
            } else {
                self.addPhotoImage.image = [self.photoArray objectAtIndex:0];
                self.addPhotoImage2.image = [self.photoArray objectAtIndex:1];
                self.addPhotoImage3.image = [self.photoArray objectAtIndex:2];
            }
            
        
    }
    
    self.priceLabel.text = self.detailItem.priceItem;
    self.titleLabel.text = self.detailItem.labelItem;
    self.descriptionLabel.text = self.detailItem.descriptionItem;
}


#pragma mark - UIImagePicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSLog(@"Photo from library %@",info);
    
    UIImage* image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    if ([self.photoArray count]<3) {
        
        
        [self.photoArray addObject:image];
        
        NSLog(@"!!!%@",self.photoArray);
        
        
        if ([self.photoArray count] == 2) {
            self.addPhotoImage2.image = [self.photoArray objectAtIndex:1];
            
        } else if ([self.photoArray count] == 3) {
            self.addPhotoImage3.image = [self.photoArray objectAtIndex:2];
            
        } else {
            self.addPhotoImage.image = [self.photoArray objectAtIndex:0];
            
        }
       
    }
    
    self.dataImage = UIImageJPEGRepresentation(image, 0.0000001);
    [picker dismissViewControllerAnimated:YES completion:nil];

    
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
    
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
    [self.view endEditing:YES];
    return YES;
}
#pragma mark - UITextViewDelegate


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    return YES;
    
    
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
    [self.view endEditing:YES];
    return YES;
    
}

#pragma mark - Actions

- (IBAction)editAction:(UIButton *)sender {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.photoArray];
    
    if (self.detailItem) {
        [self.detailItem setValue:self.titleLabel.text forKey:@"labelItem"];
        [self.detailItem setValue:self.descriptionLabel.text forKey:@"descriptionItem"];
        [self.detailItem setValue:self.priceLabel.text forKey:@"priceItem"];
        [self.detailItem setValue:data forKey:@"imageItem"];
       
    } else {
        NSLog(@"Not set");
        
    }
   
    [[[EFDataManager sharedManager] managedObjectContext] save:nil];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Excellent"
                                                                   message:@"Post is changed"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                                                }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)addPhotoAction:(UIButton *)sender {
    
    NSLog(@"ACTION");
    
    UIAlertController* alertController =
    [UIAlertController alertControllerWithTitle:nil
                                        message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* chooseFromGalleryAction =
    [UIAlertAction actionWithTitle:@"Выбрать из галереи"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               
                               UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                               imagePickerController.delegate = self;
                               imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                               
                               [self presentViewController:imagePickerController animated:YES completion:nil];
                           }];
    
    
    UIAlertAction* deletePhotoAction =
    [UIAlertAction actionWithTitle:@"Удалить"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {

                               
                               if ([self.photoArray count] != 0) {
                                   [self.photoArray removeLastObject];
                                   if ([self.photoArray count] == 2) {
                                       self.addPhotoImage3.image = nil;
                                   } else if ([self.photoArray count] == 1){
                                       self.addPhotoImage2.image = nil;
                                   } else {
                                       self.addPhotoImage.image = nil;
                                   }
                               }
                       
                           }];
    
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Отмена"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {}];
    
    
    [alertController addAction:chooseFromGalleryAction];
    [alertController addAction:deletePhotoAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    alertController.view.tintColor = [UIColor blackColor];


}


@end

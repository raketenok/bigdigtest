//
//  EFItemsTableViewController.h
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 07.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


@interface EFItemsTableViewController : UITableViewController <NSFetchedResultsControllerDelegate, UISearchBarDelegate>



@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;






@end

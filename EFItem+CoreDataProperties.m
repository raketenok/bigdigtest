//
//  EFItem+CoreDataProperties.m
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 08.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EFItem+CoreDataProperties.h"

@implementation EFItem (CoreDataProperties)

@dynamic priceItem;
@dynamic labelItem;
@dynamic imageItem;
@dynamic descriptionItem;

@end

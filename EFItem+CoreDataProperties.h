//
//  EFItem+CoreDataProperties.h
//  BIG_DIG_TEST
//
//  Created by Yevgen Yefimenko on 08.05.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "EFItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface EFItem (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *priceItem;
@property (nullable, nonatomic, retain) NSString *labelItem;
@property (nullable, nonatomic, retain) NSData *imageItem;
@property (nullable, nonatomic, retain) NSString *descriptionItem;

@end

NS_ASSUME_NONNULL_END
